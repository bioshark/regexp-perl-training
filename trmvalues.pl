# trims numbers to 2 decimals. If the 3rd decimal is not 0, it's also shown
@vals = (22.234424234, 3544.1, 54.32000233);

for (my $i=0; $i < scalar @vals; $i++) {
  $clean_val = $vals[$i];
  $clean_val =~ s/(\.\d\d[1-9]?)\d*/$1/;
  print "value $i is : $vals[$i]; cleaned: $clean_val\n";
}
