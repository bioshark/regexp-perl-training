# Converts temperatures

print "Enter a temperature, for example (22C, -4C, 43F, etc) : ";
$input=<STDIN>;
chomp($input);

if ($input =~ m/^([+-]?[0-9]+(\.[0-9]*)?)\s*([CF]?)$/i) {

  $inputNum   = $1;
  $inputType  = $3;

  if ($inputType =~ m/c/i) {
    $celsius    = $inputNum;
    $farenheit  = ($celsius * 9 / 5) + 32;
  } else {
    $farenheit  = $inputNum;
    $celsius    = ($farenheit - 32) * 5 / 9;
  }

  printf "%.2f C is %.2f F \n", $celsius, $farenheit;

} else {
  print "I don't recognise \"$input\" \n";
}
