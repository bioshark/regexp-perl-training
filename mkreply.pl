$subject="";
$date="";
$replyto="";
$sender_name="";

while ($line = <>) {
  if ($line =~ m/^\s*$/) {
    last;
  }
  if ($line =~ m/Subject: (.*)/) {
    $subject = $1;
  }
  if ($line =~ m/Date: (.*)/) {
    $date = $1;
  }
  if ($line =~ m/From: (\S+) \(([^()]*)\)/) {
    $replyto = $1;
    $sender_name = $2;
  }
  if ($line =~ m/Reply-To: (.*)/) {
    $replyto = $1;
  }
}

print "To: $replyto ($sender_name)\n";
print "From: rolland.sovarszki\@home.org (Same Motan)\n";
print "Subject: Re: $subject\n";
print "\n";

while ($line = <>) {
  $line =~ s/^/|> /;
  print $line;
}
