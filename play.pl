$text = <>;

# match between quotes
$text =~ m/( "(\\(?s:.)|[^\\*])*" )/;
# ?s:. means match everything in new line mode
print "$1\n";

# strip leading/trailing spaces.
$text =~ s/^\s+//;
$text =~ s/\s+$//;

print "$text\n";
