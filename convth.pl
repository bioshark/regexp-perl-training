# text to html converter

undef $/;
$text = <>;

# handle special characters
$text =~ s/&/&amp;/g;
$text =~ s/</&lt;/g;
$text =~ s/>/&gt;/g;

# new paragraph, replaces each empty line
$text =~ s/^\s*$/<p>/mg;

$hostnameRegex = qr/[-a-z0-9]+(\.[-a-z0-9]+)*\.(com|edu|org)/i;

# handle email addresses
$text =~ s{
  \b
  (
    \w[-\.\w]*
    \@
    $hostnameRegex
  )
  \b
}{<a href="mailto $1">$1</a>}gix;

# handle url's
$text =~ s{
  \b
  (
    http:// $hostnameRegex \b
      (
        / [-a-z0-9_:\@&?=+,.!/~*'%\$]*
        (?<![.,?!])
      )?
  )
  \b
}{<a href="$1">$1</a>}gix;


printf $text;
